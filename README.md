Visit https://growlanser6english.blogspot.com/ for more information.

[ONGOING] = This file still needs to be fully translated

[TRANSLATED] = This file has been translated and needs to be edited/proofread

[COMPLETED] = This file has been fully translated and edited (further edits may continue)
_____________________________________________________

CURRENTLY WORKING ON:

1. mako - Gem Properties Abbreviations/Descriptions (read: procrastinating)
2. risae - Translating the GL6 ELF File, inserting the Growlanser Realm script, testing the scripts, testing the GL6 VWF with Ethanol Pwned



CURRENT TO-DO LIST:

1. Implement the Gem Properties Abbreviations/Descriptions workaround --- ONGOING
2. Translate the remaining TIM2 image files --- ONGOING
4. Figuring out how to change the standard name in the 2nd naming window (Haschens naming window) --- ONGOING
5. Maybe figuring out how to expand .FLK script files? (a few files are starting to get rather large) --- ONGOING
6. Translate the going-into-a-city-name files, which i believe are image files --- ONGOING
7. Debugging the GL6 VWF
8. Reverse engineering the ELF file to allow for better translations
9. Adding subtitles to .mpg movie files (adding subtitles works, but sound doesn't work anymore)



CURRENT PROGRESS:

1. Menus (minus gem abbrev./desc.) --- DONE
2. SCEN scripts ---
	- prologue --- DONE
	- chapter 1 --- DONE
3. Inserting Growlanser Realms script --- 40%
4. GL6 MOV --- 0/XX
5. tm2 texture files ---
6. ELF file --- 85%
7. Updating the GL6 .fnt font to a better font --- DONE (imported from GL5 with the GL6 VWF code)
8. Extract scene scripts --- DONE